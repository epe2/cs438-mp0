# MP0
Introduction to UNIX Network Programming with TCP/IP Sockets

## Assignment
Connect to `cs438.cs.illinois.edu` on port `5900`. Establish a connection to the TCP Server on the requested port. Once the socket is created, perform the handshake specified below. `c` is the application.

### Handshake
```
c: HELO\n
s: 100 - OK\n
c: USERNAME <username>\n
s: 200 - Username: <username>\n
```

### Data Exchange (10x)
```
c: RECV\n
s: 300 - DATA: <some_string>\n
```

### Closeout
```
c: BYE\n
s: 400 – Bye\n
```

## Development Environment Setup

### Prerequisite
- Any MacOS or Linux machine
- [Docker CE](https://docs.docker.com/install/)

### In a shell
```sh
# Run Docker Container
$ ./deploy.sh

# Stop container and cleanup
$ ./clean.sh
```

## Files
- `*.c, *.h`: Source Files
- `deploy.sh`: Build and run Dockerfile
- `launch.sh`: Start script to run _inside_ Docker container
- `clean.sh`: Kill Docker container, remove images
- `Makefile`: C/C++ compiler Instructions, run on container creation
- `Dockerfile`: Setup the Docker container for use, _"infrastructure as code"_
