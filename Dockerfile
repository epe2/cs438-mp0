FROM gcc:9.1.0
COPY . /usr/src/mp0
WORKDIR /usr/src/mp0
RUN make all
CMD ["./launch.sh"]
