#!/usr/bin/env bash

# Starts up processes in Docker container

echo "Starting MP0..."

./server &
sleep 0.5
./client 127.0.0.1

./listener &
sleep 0.5
./talker 127.0.0.1 "Hello World"
