#!/usr/bin/env bash

# Source: https://hub.docker.com/_/gcc/?tab=description

docker build -t mp0 .
docker run -it --rm --name mpo-running mp0

# Make inside container:
# docker run --rm -v "$PWD":/usr/src/mp0 /usr/src/mp0 gcc:9.0.1 make
